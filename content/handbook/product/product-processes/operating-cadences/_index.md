---
title: Product Operating Cadences
description: >-
  Recurring meetings and other regular operating cadences
---

### Team Meetings

Below you'll find overviews for team meetings designed to boost Product team knowledge, communication, and collaboration. Our goal is to have minimal but valuable sync meetings for the Product team. All sync Product team meetings are required to have agendas and recordings that support async collaboration. We encourage any Product team member to propose a new meeting or the re-design of an existing meeting to help drive or improve collaboration amongst the Product team in an effort to drive more product value for our users.

#### Adding or revising Product team meetings

To ensure transparency and alignment on the design and frequency of team meetings requiring time commitment from the entire or broader Product team, please raise an MR on this page, requesting review and collaboration from Product Operations, the CPO and any other DRIs noted for the meeting. Product Operations should be always be included to review the addition of new or major revision of existing team meetings because they monitor the overall flow, productivity and satisfaction of all Product team meetings. "Broader" Product team refers to meetings that affect team members across Sections, Stages, and Groups on a regular cadence. For example, if a meeting is proposed that requires the participation of **all** Group Product Managers on the Product team **every month**.

The intent of this review and collaboration workflow is not to "block" but to help ensure team meetings compliment and flow well together, taking into consideration the following:

- the existing macro Product team workflow
- non-duplication of existing sync or async meetings
- support from Product Operations for successful rollout to and adoption by the Product team

#### Product Management Meeting (Bi-weekly)

**DRI:** [Product Operations](/handbook/company/team/#fseifoddini) and [Chief Product Officer](/handbook/company/team/#david)

- **What:** A bi-weekly team meeting for Product. All team members can add items to the agenda either as a Read-Only, or as a discussion item.
- **Who:** Product along with interested parties across departments. The meeting is open to all GitLab team members.
- **When:** Every other Tuesday for 50 minutes. The start time alternates each week between <time datetime="15:00">3 pm UTC (10 am ET / 7 am PT)</time> and <time datetime="21:00">9 pm UTC (4 pm ET / 1 pm PT)</time> to accommodate timezones.
- **Format:** The agenda is curated and moderated by Product Operations in partnership with the Chief Product Officer
- **Recordings:** The meetings are set as Private Recordings and are saved to the [Product Team](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko7HO427nXkoz9kovi_2dBi) playlist on YouTube Unfiltered. If you are unable to view a video, please ensure you are logged in as [GitLab Unfiltered](/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube).

**Potential Topics:**

1. `READ-ONLY` announcements which require no synchronous discussion and are for information sharing only. If any action is required, this should be highlighted specifically by the reporter.
1. New hire announcements, bonuses, promotions and other celebrations.
1. Announcements and discussion on department-wide changes (e.g. OKRs, performance indicators, pricing changes).
1. Learning about the product: showcasing different product areas, their direction, why they matter to GitLab.
1. Iteration: helping your peers break down issues and designs into smaller steps, across the department.
1. **Guest Speakers:** We encourage product managers to nominate guest speakers by reaching out to [Product Operations](/handbook/product/product-operations/) directly with a request.

All product team members are encouraged to add agenda items. The Chief Product Officer will prioritize the discussion items, and any items we can't get to in the allotted 50 min will be moved to the following meeting. If you are a Product team member and are unable to attend the call, you may add items for `READ-ONLY` to the agenda.

#### Product Leadership Meeting (Weekly)

**DRI:** [Chief Product Officer](/handbook/company/team/#david) and  [EBA to Chief Product Officer](/handbook/company/team/#gschwam)

The Chief Product Officer and their direct reports track our highest priority Product Team initiatives. If one of those initiatives needs to be discussed synchronously the assigned individual should add it to the meeting's [GoogleDoc agenda](https://docs.google.com/document/d/1yN2n1ei24HiM5G7tBxaqK4yNPYreKpLZ3CG3nsEFOR4/edit#heading=h.384zmpcao5l6). Directors can delegate and coordinate cross-product initiatives as needed based on the issues in this board.

As part of this Product Leadership Meeting we also [review progress towards our OKRs](https://gitlab.com/gitlab-com/Product/issues/187).

Non-public items for discussion should be added directly to the [agenda document](https://docs.google.com/document/d/1yN2n1ei24HiM5G7tBxaqK4yNPYreKpLZ3CG3nsEFOR4/edit#heading=h.384zmpcao5l6) for the meeting.

#### Section Performance Indicator Review (Monthly)

**DRI:** Appropriate [Product Section](/handbook/product/categories) Leader & [Senior Director, Product Monetization](/handbook/company/team/#justinfarris)

- **What:** A monthly meeting for [Product  Sections](/handbook/product/categories/#devops-stages) to provide updates on  Performance Indicators, inclusive of their Stages and Groups and the cross-functional team. The main deliverable from the team is for each group to clearly show what they are doing in the next month to drive their metrics in the right direction.
- **When:** During the last two weeks of every month
- **Duration:** 50 minutes
- **Format:** The meeting is lead by the Section Leader
  - All data reviewed will be published on the Section's PI page in the internal handbook ([example](https://internal.gitlab.com/handbook/company/performance-indicators/product/fulfillment-section/). Sections may optionally provide supporting slides.
  - Content will focus on the following areas:
     1. Development - Error Budgets, Past Due Security/InfraDev Issues (S1/S2)
     2. Quality - Past Due Bugs (S1/S2)
     3. UX - Past Due SUS Impacting Issues (S1/S2)
     4. Product - XMAU, XFN Prioritization
- **Recordings:** Each meeting will be recorded, and published so participants can engage async. The meetings have [REC] in the title and will automatically go to the [Google Drive Folder](https://drive.google.com/drive/search?q=type:video%20Performance%20Indicator). If you cannot find a recording, please message the EBA to the Product Division.
- **Preparation:** Section Leaders will provide the content at least 2 business days in advance of their review by posting a link to the updated PI pages and slides (optional) in the meeting agenda to allow ample time for meeting participants to review in advance. The Chief Product Officer may not attend the sync meeting but will always review async.
- **Meeting Attendees**
- The following participants will be included in the sync meeting from each group (attendance can be async):
   1. Engineering Managers or Sr Engineering Manager / Director for the stage
   2. Product Design Manager(s) for the Stage
   3. Quality Engineering Manager for the Stage
   4. Group Manager, Product for the Stage
   5. Product Manager for the Group
- All Sections  will leverage and personalize this [template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/section-PI-review-monthly-template.md)
- All Sections will leverage and personalize this [agenda template](https://docs.google.com/document/d/1hKGU8hN_SpudlV-SnCu821EmMwgLVppJsXZmY4Gom4A/edit).

#### Product Direction Showcases

**DRIs:** [Director, Verify & Package](/handbook/company/team/#jreporter) and [Product Operations](/handbook/company/team/#fseifoddini)

- **What:** A monthly meeting for 4 stages to present 10-minutes on their direction as well as provide a deep dive or demo into a recent product delivery.
- **When:** Monthly, rotating between EMEA and NA Timezone
- **Duration:** 50 minutes
- **Format:** The meeting is lead by the Group Manager of the Stage.
- **Recordings:** The meetings have [REC] in the title and will automatically go to the [Google Drive Folder](https://drive.google.com/drive/search?q=type:video%20Product%20Direction%20Showcase). If you cannot find a recording, please message the EBA to the Product Organization.
- **Preparation:** A monthly issue will get created and 2 weeks before meeting Stage leaders will populate their place in the meeting and will provide a link to a deck or direction page that will be reviewed for their timeslot.

### Kickoff meetings

The purpose of our kickoff is to communicate with our community (both internal and external) a view of what improvements are being planned in the coming release. This can help other GitLab teams, community contributors and customers gain an understanding of what we are prioritizing and excited to deliver in our next iteration.

While kickoffs are commonly used to ensure visibility of work across an internal audience, our connection to a broader community and commitment to transparency means we also serve an external audience.

The process for preparing and presenting group and company wide Kickoff meetings is outlined in our [Monthly Kickoff issue template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Monthly-Kickoff.md). We create an issue from that template each month to collaborate on that month's Kickoff.

The calendar invite for the monthly kickoff meeting is scheduled for the 18th of each month. When the 18th falls on a Saturday or Sunday, the kickoff date is moved to the following Monday, which is either the 19th or 20th of the month.
