---
aliases: /handbook/legal/ESG
title: "ESG"
description: "Information and processes related to ESG"
---

The Environmental, Social, and Governance (ESG) [Team](/job-families/legal-and-corporate-affairs/environmental-social-governance/) creates and maintains GitLab's Corporate Sustainability strategy and programs. This includes ESG disclosures and public ESG reporting, identifying and prioritizing key issues to advance GitLab's social and environmental goals, and creating partnerships with nonprofit organizations that support GitLab's values and mission.

## ESG Strategy

Environmental, social, and governance (ESG) practices, although newly formalized in GitLab Inc.'s ("GitLab", "we", "our") strategy, have been embedded into our work culture since our inception. Deeply integrated into our business philosophy, GitLab's ESG strategy is driven by our [values](/handbook/values/) of Collaboration, Results, Efficiency, Diversity, Inclusion and Belonging, Iteration, and Transparency (CREDIT).

In December 2022, we conducted an ESG materiality assessment to determine which ESG topics are most important to our business and our stakeholders. Through engagement with both internal and external stakeholders, we explored which ESG topics have the greatest impact on GitLab's business, and where we have the potential to have the greatest impact on the environment, society, and our global communities. Our materiality matrix was finalized in January 2023.

In 2023, we released our inaugural [FY23 ESG Report](/handbook/company/esg/) in the Handbook. The report includes information on our key ESG focus areas, our programs and policies, acheivements to date, metrics and targets, and plans for the future.

## ESG Request Process

### Philanthropic Requests

Information on how GitLab Inc. supports Registered Nonprofit Organizations can be found in the [Philanthropy Policy](/handbook/legal/philanthropy-policy/)

Please note that for all Philanthropic Requests, including requests for GitLab to join as a member to an association, program or organization, approval by the ESG team and CLO is required as defined by the Oversight Responsibility section of the Policy.

If you would like to submit a philanthropic request, please follow the instructions based on your request type.

#### Monetary Contributions

There are two ways that team members can submit a request for monetary support.

1. Request funding from the ESG team to support a Registered Nonprofit Organization OR
1. Request utilizing department or TMRG  budget to support a Registered Nonprofit Organization

If you are requesting funding from the ESG team to support a Registered Nonprofit Organization, please note that at this time, we are not accepting applications. If you would like to submit a Nonprofit Organization to be considered for support in the future, please go to the [Philanthropic Requests epic](https://gitlab.com/groups/gitlab-com/-/epics/2145) and open a new issue using the [Monetary_Support Template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=monetary_support).  You will be notified if there is a future opportunity.

If you have a department or TMRG  budget that you would like to utilize to support a Registered Nonprofit Organization, please go to the [Philanthropic Requests epic](https://gitlab.com/groups/gitlab-com/-/epics/2145) and open a new issue using the [Monetary_Support Template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=monetary_support). Please tag your manager to approve the request if you are submitting on behalf of your department. If you are submitting a request on behalf of a TMRG or DIB, please add Sherida McMullan as a reviewer.
Please allow a minimum of 10 working days for review.

Once all necessary approvals have been documented for the charitable contribution request, if the donation is >$5,000, the requester will need to set the recipient up in Coupa and follow the PO process in order to issue payment. Please [create a purchase request](/handbook/finance/procurement/#-how-do-i-create-a-purchase-request-in-zip) in Zip to get the organization registered and to process payment. If the donation is equal to $5,000 or <$5,000, please contact AP@gitlab.com to process payment as an exception to the PO process. Please note that if it is a new recipient that we have not paid before, an invitation to register in Coupa will be sent to the main contact on file to get their payment & tax information set up in the system.Please note that the requester is responsible for contacting the AP team to make the donation. Once the donation is made, the requester should close the issue. If the requester needs assistance with the payment, contact the ESG team.

#### GiveLab

GiveLab is GitLab's Team Member Volunteer Program. The GiveLab Program lives under the [ESG Team](/job-families/legal-and-corporate-affairs/environmental-social-governance/) and is a part of GitLab's overall [ESG strategy](/handbook/legal/esg/#esg-strategy) and program development. The term GiveLab encompasses all Team Member Volunteering initiatives at GitLab.

GitLab encourages team members to take part in volunteer initiatives that support their local communities, participate in virtual volunteer activities, and organize volunteer activities as part of team events and get togethers.

Team members may self organize volunteer events at any point throughout the year. To submit a request for a team volunteer activity with a [Registered Nonprofit Organization](/handbook/legal/philanthropy-policy/#who-we-support) that isn't on the current [GiveLab Nonprofit Directory](https://docs.google.com/spreadsheets/d/1koFKQbKqm4jpKtZlteLDTPM3HYv20nffXaqRJ2C8YBA/edit?gid=0#gid=0), please go to the [Philanthropic Requests epic](https://gitlab.com/groups/gitlab-com/-/epics/2145) and open a new issue using the [Volunteer_Support Template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=volunteer_support).

Team members can also request support from the ESG Team to organize local or virtual volunteer opportunities on their behalf by going to the [Philanthropic Requests epic](https://gitlab.com/groups/gitlab-com/-/epics/2145) and opening a new issue using the [Volunteer_Support Template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=volunteer_support). Please write "yes" for the question, "Would you like the ESG team's help organizing the volunteer activity?"

All team members and volunteer activities must adhere to the [GitLab Philanthropy Policy](/handbook/legal/philanthropy-policy/). Team members must follow GitLab's paid time off (PTO) policy if volunteering during work hours and use the "public service/volunteer" option in Workday.

If volunteering in person, team members may incur some expenses. All expenses should be submitted in Navan using the "GiveLab" classification. Please note that GitLab does not allow team members to travel to in-person volunteer events. All in-person volunteering should be local to the team member.

As with all [company expenses](/handbook/finance/expenses/#1-policy), team members must be thoughtful in spending the company's money and use best judgment to ensure that all expenses are deemed "ordinary and necessary." Team members should follow all [team member expense responsibilities](/handbook/finance/expenses/#2-responsibilities).

Expenses allowed for reimbursement (for in-person volunteer events):

- Local travel only -  [mileage](/handbook/finance/expenses/#mileage), [train](/handbook/finance/expenses/#train), [taxi/rideshares](/handbook/finance/expenses/#transportationparking). Must follow and comply with the current limits of our Expense Policy. Code under their individual travel category and use the "GiveLab" classification in Navan
- [Parking](/handbook/finance/expenses/#transportationparking) for the event.  Must follow and comply with the current limits of our Expense Policy.  Please use the "GiveLab" classification in Navan
- [Meal](/handbook/finance/expenses/#meals-company-provided-while-traveling) during the event.  Must follow and comply with the current limits of our Expense Policy.  Code under Travel-Meals for Myself and use the "GiveLab" classification in Navan

Expenses not allowed for reimbursement:

- Any airfare, lodging, rental cars, coworking space, internet expenses are not allowed
- Any supplies or items needed for the event
- Donations to the nonprofit
- Any expenses that are not mentioned as "allowed" above are not reimbursable and there will be no exceptions

As with our unique ways of working, GitLab and its team members have identified and sought out opportunities for impact that speak not only to our [values](/handbook/values/) but also to our [all-remote](/handbook/company/culture/all-remote/guide/) nature. To review previous opportunities that team members participated in, visit the [historical activities page](/handbook/people-group/givelab-volunteer-initiatives/#gitlab-donation-drives).

#### GiveLab Nonprofits

We have created an internal [GiveLab Volunteer Directory](https://docs.google.com/spreadsheets/d/1koFKQbKqm4jpKtZlteLDTPM3HYv20nffXaqRJ2C8YBA/edit?usp=sharing) that features a list of vetted nonprofit organizations with available volunteer opportunities. Team members can search the document for virtual volunteer opportunities, opportunities to volunteer with [GitLab Foundation](https://www.gitlabfoundation.org/) grantees and search for local opportunities.

We encourage all team members to contribute to our GiveLab Volunteer Directory. To recommend a nonprofit organization to add to the Directory, please open a [Volunteer Recommendation Issue](https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/master/.gitlab/issue_templates/volunteer_recommendation.md).

#### GiveLab Champions

**GiveLab Champions overview**

The GiveLab Champions are team members who are passionate about giving back to their communities and want to encourage other team members to do the same.

GiveLab Champions self-identify to participate in the voluntary Champions group, managed by the ESG team. The GiveLab Champions help activate the GiveLab signature program (launching Fall of 2024), but also work to organize and promote volunteer opportunities year-round.

**The importance of GiveLab Champions**

GiveLab Champions help team members build trust through social connections, build connections within their communities, and help GitLab provide meaningful opportunities for team members to give back. The GiveLab Champions ensure global voices are heard and relevant causes are represented based on where team members live. GiveLab Champions help to make GitLab a better place to work.

**Team member benefits of volunteering**

- Champions introduce new and current team members to GitLab's culture and help to build and maintain engagement
- Champions help build trust through social connections
- Champions have the opportunity to develop professional and leadership skills, build relationships across the company and make a difference at work and in local communities
- Champions can network and provide positive impacts to the business

**GiveLab Champion responsibilities**

- Recommend local [Registered Nonprofit Organizations](/handbook/legal/philanthropy-policy/#who-we-support) that meet GitLab's eligibility criteria for volunteer activities
- Help organize local volunteer events for team members to participate in
- Provide recommendations to the ESG Team for virtual volunteer opportunities
- Encourage team members to volunteer
- Help plan and execute the GiveLab signature program
- Participate in quarterly Champion group meetings
- Provide feedback to the ESG team on the GiveLab and ESG Programs

**Time commitment**

The time commitment for a GiveLab Champion is estimated to be 3-5 hours per quarter. Participation can vary throughout the year.

**Selection criteria**

- The team member must be performing in their role and not part of any performance management process as confirmed by the People Group
- Must be a full-time GitLab team member. This includes team members employed through a PEO or who contract with CXC or directly with GitLab IT BV
- At least 3-month tenure at GitLab
- Approval from your direct manager and support to commit the time and to use this leadership role as a professional development opportunity
- A minimum one-year commitment in the role but understanding this may change to less and can be more

**Selection process**

- Interested team members should complete the [GiveLab Champions survey](https://docs.google.com/forms/d/e/1FAIpQLSf1Nx999ruN-_Ja0T6nd-ClkNOMRXNEEj-P40R7rQ64iV7ABQ/viewform) expressing their interest
- Team members must confirm that they have approval from their manager to participate
- The ESG Team evaluates interest at a minimum on a monthly cadence and will reach out to the interested individual for follow up questions

#### In Kind Support

**GitLab for Nonprofits**

[GitLab for Nonprofits](https://about.gitlab.com/solutions/nonprofit/) is an in-kind donation program that provides licenses and seats at no cost to eligible Registered Nonprofit Organizations.

GitLab for Nonprofits operates on a first come first served basis. Once the [annual donation of 5,000 seats](/handbook/legal/philanthropy-policy/#2-in-kind-product-donation) is met, the application will remain closed for the year.

**FAQs**

**What are the benefits of the GitLab for Nonprofits program?**

GitLab is a single platform for project management, collaboration, source control management, git, automation, security, and much more. Because it is easy to use, flexible, and all in one place, it is the best choice for nonprofits to scale their work. The GitLab for Nonprofits Program gives free licenses of GitLab to registered nonprofit organizations.

Nonprofits accepted into the program will be provided a free Ultimate license for one year (SaaS or self-managed) for up to 20 seats. Additional seats may be requested although they may not be granted.

**Who qualifies for the program?**

GitLab supports Registered 501c3 (or jurisdictional equivalent) Nonprofit Organizations in good standing that align with our [Values](/handbook/values/). A "Registered Nonprofit Organization" is one that has been registered with the local government or authorized agency within its applicable local, state, provincial, federal or national government.

GitLab prioritizes Registered Nonprofit Organizations that help advance GitLab's social and environmental key topics that were defined in GitLab's [2022 materiality assessment](/handbook/legal/esg/). GitLab's current social and environmental key topics are:

- Diversity, Inclusion, and Belonging
- Talent Management & Engagement
- Climate Action and Greenhouse Gas Emissions

For the calendar year 2024, we will limit the in-kind program to 5,000 seats, which was approved by finance and the board in the [Philanthropy Policy](https://docs.google.com/document/d/1b40Z-5uVebZ05RQehn5qq86c2qP_0w8T40MrygP3VEU/edit). Each organization will be eligible for up to 20 seats. This will allow us to assist as many organizations as possible. This will be revisited throughout the year and adjusted as needed. Interested organizations who are new customers may request additional seats although the request may not be granted. To limit churn, current GitLab customers that apply to transition to the Nonprofit Program will not be granted a special request above the 20 seats.

**What are the terms of the GitLab for Nonprofits program?**

Upon acceptance, program members are subject to the [GitLab Subscription Agreement](/handbook/legal/subscription-agreement/). The decision to issue a GitLab for Nonprofits license is always at the sole discretion of GitLab.

**How does someone apply?**

Interested organizations need to visit the [GitLab for Nonprofits](https://about.gitlab.com/solutions/nonprofit/) page and submit the [application form](https://about.gitlab.com/solutions/nonprofit/join/#nonprofit-program-application).

**How are applications processed?**

1. Nonprofits apply on the [GitLab for Nonprofits](https://about.gitlab.com/solutions/nonprofit/join/#nonprofit-program-application) page. Once the application is submitted, the Nonprofit will receive a message and a link to TechSoup, our verification partner.
2. The Nonprofit will then need to log in or create their TechSoup account. TechSoup provides a rigorous vetting process to ensure the nonprofit is eligible for the GitLab for Nonprofits program and meets all requirements.
3. If a Nonprofit is verified, TechSoup will notify GitLab.
4. GitLab will then undergo its own vetting and approval process.
5. Once all parties have verified and approved the Nonprofit, GitLab will send the instructions directly to the Nonprofit to redeem their license.
6. If a Nonprofit is not verified through TechSoup, TechSoup will provide details on how the Nonprofit can become verified.
7. If a Nonprofit is declined from GitLab, GitLab will notify the Nonprofit via nonprofits@GitLab.com.
8. Please allow up to 15 business days for the application and verification process.

**Must Nonprofits renew their memberships?**

Yes. All nonprofits must renew their membership annually, which involves a re-verification process. Nonprofits will submit for renewal the same way they first applied for the program.

**Where can members receive support?**

While GitLab for Nonprofits Program benefits do not include product [support](https://about.gitlab.com/support/), program members can receive help with GitLab in a number of ways. In general, we recommend the following:

- Send questions or issues requiring immediate attention or sensitive information to the GitLab for Nonprofits team at Nonprofits@GitLab.com
- Review [GitLab Docs](https://docs.gitlab.com/) for answers to general product-related questions.
- Post questions in the [GitLab Forum](https://forum.gitlab.com/), where community members and GitLab team members can review and discuss them.
- File bug reports and breaking behaviors [as issues](https://gitlab.com/gitlab-org/gitlab/-/issues) for product teams to review and address.

**I'm a GitLab Team Member and I have a customer applying for the program. What do I do?**

- Please point the Nonprofit to the GitLab for Nonprofits page to learn about the program, eligibility and application.
- If the Nonprofit has questions on the eligibility or status of an application, please advise them to reach out to nonprofits@gitlab.com. Or team members can inquire about the status of an application in #esg.
- If you have a question or concern about the customer churn, please raise this in #esg and an ESG team member will contact you.

#### Matching Gifts Program

 At this time, GitLab does not offer a matching gifts program.

### Membership Requests (non-monetary)

For requests related to GitLab Membership of Association, Program or Organization, and includes terms, conditions and/or obligations on GitLab that must be executed, please follow the below process.
Open an [issue using the Membership Request Issue Template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=membership-request). Complete and attach the necessary information. Note: If you are submitting a request on behalf of a TMRG or DIB, please add Sherida McMullan as a reviewer

NOTE: For any request(s) that require payment, please be certain to follow applicable ESG & Procurement processes.

## ESG Materiality Matrix

Six key topics were identified in GitLab's materiality assessment. These key topics will drive GitLab's ESG strategy and program development. This page will continue to be updated as we make progress towards developing plans and programs to advance our ESG goals.

![2023 Materiality Matrix](/handbook/legal/esg/2022-materiality-matrix-062123.png)

Here are GitLab's current key topics with their drivers:

### Environment

#### Climate action and greenhouse gas emissions

- Measurement and reduction of scope 1, 2 and 3 emissions
- Greenhouse gas reduction targets, goals and commitments

### Social

#### Diversity, inclusion and belonging

- Diverse hiring and recruitment of underrepresented groups
- Culture of DIB (events, TMRGs, courses, etc.)
- Pay and promotion equity and inclusive benefits
- Board, leadership and workforce diversity KPIs
- Product accessibility

#### Talent management and engagement

- Talent attraction and recruitment
- Team member learning and development
- Leadership programs
- Succession planning
- Talent retention
- Workplace culture and remote work
- Comp and benefits

### Governance

#### Cybersecurity and data privacy

- Data, system and network breaches
- Monitoring of emerging threats
- Information security training
- Customer data use transparency
- Data processing and storage
- PII

#### Business ethics

- Up to date code of conduct, anti-corruption, anti-bribery policies
- Regulatory compliance
- Legal proceedings and monetary losses
- Internal compliance
- Oversight and ethics training
- Human rights policies, risk assessments and due diligence

#### Responsible product development

- Open source
- Ethical AI
- Human rights issues from product use
- Environmental impact considerations for product
- Data processing and storage

## Our Progress

Please read our [ESG report](/handbook/company/esg/) to learn about our progress.

## ESG Training

To learn more about ESG at GitLab, please take our ESG Training course available on [LevelUp](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/course/esg-training) and in the [Handbook](/handbook/legal/esg/ESG-Training/).

## Measuring Results

Disclosing our progress through data aligns with our [Transparency](/handbook/values/#transparency) and [Results](/handbook/values/#results) values. We have a section in our Handbook devoted to [Key Performance Indicators](/handbook/company/kpis/) (KPIs) where we update our progress regularly. Every part of GitLab has KPIs linked to the company Objectives and Key Results [(OKRs)](/handbook/company/okrs/). As we build our ESG strategy, we will continue to add and update relevant ESG KPIs to the handbook.

## FAQ

**Q: Who can I contact for ESG-related questions?**

A:  Senior Director, ESG/DRI: Stacy Cline - @slcline on GitLab.
Email: ESG@GitLab.com.

**Q: Does GitLab calculate its carbon emissions?**

A: Yes, GitLab completed its first greenhouse gas (GHG) inventory covering emissions from FY23. Please view the results of our inventory [here](/handbook/company/esg/#climate-action-and-greenhouse-ghg-emissions) and our third-party assurance letter [here](chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/https://about.gitlab.com/documents/GitLab_FY2023_Verification_Opinion.pdf).

**Q: Has GitLab set a Science Based Target (SBT)?**

A: Not yet. GitLab completed its first [GHG inventory](/handbook/company/esg/#climate-action-and-greenhouse-ghg-emissions) and will use the results to inform a reduction plan and target.

**Q: Does GitLab track employment by gender and ethnicity?**

A: Yes, [view the most recent identity data](/handbook/company/culture/inclusion/identity-data/).

**Q: Does GitLab set goals to increase diversity?**

A: Yes, GitLab publishes [people success KPIs](/handbook/people-group/people-success-performance-indicators/#executive-summary).

**Q: How does GitLab define underrepresented groups?**

A: An underrepresented group describes a subset of a population that holds a smaller percentage within a significant subgroup than the subset holds in the general population. View the full definition [here](/handbook/company/culture/inclusion/#examples-of-select-underrepresented-groups).

**Q: Is GitLab certified as a diverse supplier?**

A: GitLab is a publicly traded company (NASDAQ: GTLB) and is not defined as a diverse supplier and is unable to be certified as such, accordingly. Nevertheless, diversity, inclusion, and belonging (DIB) is a [core value](/handbook/values/) at GitLab. On a daily basis, we strive to keep our operations, employment practices, and supplier selection in line with [this value](/handbook/company/culture/inclusion/). GitLab's [DIB team](/job-families/people-group/diversity-inclusion-partner/) builds an environment where all team members feel a [sense of belonging](/job-families/people-group/diversity-inclusion-partner/), which results in a truly inclusive and welcoming work environment. Moreover, GitLab's Procurement team selects potential suppliers with [responsible sourcing and diversity](/handbook/finance/procurement/) in mind.

**Q: I'm a GitLab team member and I received an ESG questionnaire through an RFP. What do I do?**

A: Please follow the steps outlined in the [RFP process](/handbook/security/security-assurance/field-security/field-security-rfp/).

**Q: Does GitLab sponsor/fund nonprofit organizations?**

A: GitLab does not currently have a formal corporate giving program. Pending the results of the 2022 materiality assessment, the first iteration of a corporate giving program is planned for 2023.

**Q: Does GitLab donate its product to nonprofits?**

A: GitLab does not currently have a formal in-kind donation program. Pending the results of the 2022 materiality assessment, the first iteration of a non-profit in-kind donation program is planned for 2023. Learn about [other ways](/handbook/marketing/developer-relations/community-programs/education-program/) that GitLab contributes its product to the community.
