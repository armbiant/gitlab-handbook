---
title: "Asana"
description: "Asana is a collaborative work management platform that will support GitLab's mission to enable everyone to contribute to and co-create the software that powers our world. GitLab's Marketing team is planning to use Asana to track projects (e.g., Product Launches), connect work to goals, and coordinate work across the team. Asana will have access to Orange data including pre-release product launch information and GitLab issues and epics in private projects."
---

## About Asana

[Asana]](https://app.asana.com/) is a collaborative work management platform that will support GitLab's mission to enable everyone to contribute to and co-create the software that powers our world. GitLab's Marketing team is planning to use Asana to track projects (e.g., Product Launches), connect work to goals, and coordinate work across the team.

## Why?

Our team is using Asana to add a made-for-marketing project and task management tool so that we can streamline and simplify projects and processes, improve efficiency, collaboration, and transparency, and free up time for our team to better understand how our customers use GitLab.

We've consistently heard from team members across the marketing org that there is an opportunity to improve efficiency. The main problems we are looking to solve are:

- Stretching GitLab to perform as a project management tool for Marketing processes it was not built for, and use cases it will never be used for by our customer base. 
- This can cause undue administrative work, bottlenecks and detracts from our day-to-day commitment to efficiency. 
- Adding a made-for-marketing project and task management tool will improve efficiency, freeing up time for our team to learn about and understand how our customers use GitLab

## Users

Asana will be rolled out across the entire Marketing Org, excluding Sales Development and the Data Team. 

## Rollout

The Marketing Ops team started implementation on 2024-07-12. There is an ongoing project in [Asana](https://app.asana.com/0/1207801099246898/1207801099246898) that we will be working off of.

We will be starting with the teams that can benefit the most and have the most impact first, and will continue to roll out from there.
