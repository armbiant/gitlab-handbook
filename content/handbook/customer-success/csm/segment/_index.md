---
title: CSM Segments
---

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM-related handbook pages.

---

## Overview

The Customer Success Manager organization is distributed across different customer segments, following a regional approach and Annual Recurring Revenue.

## Segments

[<button class="btn btn-primary" type="button">Digital Touch</button>](/handbook/sales/field-operations/customer-success-operations/cs-ops-programs/)
[<button class="btn btn-primary" type="button">CSE</button>](scale/)
[<button class="btn btn-primary" type="button">AMER CSM</button>](amer/)
[<button class="btn btn-primary" type="button">EMEA CSM</button>](emea/)
[<button class="btn btn-primary" type="button">APJ CSM</button>](apj/)
[<button class="btn btn-primary" type="button">CSA</button>](csa/)
