---
title: "IT Enterprise Applications"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## <i class="fas fa-users" id="biz-tech-icons"></i> About Us

The **IT Enterprise Applications Team** is responsible for the design, implementation, compliance and operations of specialized applications that are key to the digital transformation by streamlining business processes, improving efficacy and efficiency, and enabling organizational growth
We are directly responsible for all of GitLab's finance systems and Enterprise Applications Integrations. We build and extend these applications to support the processes of our business partners and rationalize our application landscape to ensure it is optimized for efficiency and spend.
Our team ensures the availability of these applications and integrations through monitoring and alerting. These internal-facing applications include a multitude of different applications and environments, including Zuora, Adaptive Planning, NetSuite, Navan Expense, etc. We are also responsible for the IT Audit and Compliance function to ensure we pass SOX Audit for our IT General Controls (ITGC).

## <i class="fas fa-users" id="biz-tech-icons"></i> Vision

- Enable end to end business processes within the enterprise applications that seamlessly hand off to each other and ensure it provides a great user experience to our business partners
- Ensure data integrity between systems and security of that data
Constantly iterate to simplify and ensure processes are efficient and automated as much as possible.
- Leverage out of the box best practices as much as possible. We buying and extend applications where we don’t see building them as GitLabs core engineering competency
- IT Audit and Compliance - Ensuring that all customer / business data is secure and can pass key audits for attestations and compliance with SOX, SOC, etc.

## <i class="fas fa-users" id="biz-tech-icons"></i> Strategy

Our department focuses on managing the framework of how GitLab procures, implements, integrates, secures, exports analytics, and supports our [tech stack applications] (https://handbook.gitlab.com/handbook/business-technology/tech-stack-applications/) while providing strategic enablement and integration support for all departments and team members that allows us to scale efficiently, [SAFEly] (https://handbook.gitlab.com/handbook/legal/safe-framework/), and securely.

## <i class="fas fa-users" id="biz-tech-icons"></i> Our Teams

- CRM Systems Services
- G&A Services
- Program Management & Strategic Planning
- Integrations, Automations, & Architecture
- EntApps Services
- SOX Compliance

## <i class="fas fa-users" id="biz-tech-icons"></i> Our Services

### CRM Systems Services

We support the GitLab field organization by providing reliable, scalable, and intuitive technology platforms for everyday use. Primarily working on Salesforce and its related revenue systems, our goal is to constantly deliver value in the form of features to our end users. We also act as the connective tissue between business and technology, gathering requirements from our internal customers, designing the technical specifications and executing on the delivery of the solution.

### G&A Services

We are directly responsible for all of GitLab’s finance systems and Enterprise Applications Integrations. We build and extend these applications to support the processes of our business partners and rationalize our application landscape to ensure it is optimized for efficiency and spend. Our team ensures the availability of these applications and integrations through monitoring and alerting. These internal-facing applications include a multitude of different applications and environments, including, NetSuite, Navan, Zip, Coupa, etc.

### Program Management & Strategic Planning

We provide a standard approach to project delivery across the IT department by providing full and accurate visibility of IT project status. We also effectively prioritize 

project management resources to support all IT initiatives. Our services consist of resource and capacity planning, “T-shirt” sizing, deployment management and IT portfolio alignment. The PMO team will firm up requirements, use cases, and process flows as we implement systems, enhance them or deliver fixes.

### Integrations, Automations and Architecture

Our architecture team manages all of the integrations and automations between Enterprise Applications at GitLab. Focusing on building out failover, redundant and auditable integrations that are constantly monitored. Developing automations to eliminate manual and repetitive tasks along with contributing to improved efficiency, service quality, and overall organizational performance.

### IT Services & Operations

Enterprise Application Services (EAS) provides  horizontal support, operations, administration, governance, risk assessment, and compliance of Enterprise Services. Leveraging the ITIL framework, EAS is responsible for the end-to-end service and application lifecycle management, and promotes operational excellence through Enforcement of IT Policies & Procedure, including Change-, Release-, and Incident Management, while ensuring operating effectiveness through Compliance Reviews.

### SOX Compliance

We are the single point of contact for IT owned applications for ITGCs. We participate in walkthroughs hosted by KPMG or Internal Audit. We support operating effectiveness testing by KPMG and IA. Facilitate ITGC remediations along with onboarding of new applications owned by IT. We facilitate SOX UARs for systems and key reports as well as SOD reviews for key SOX financial applications.
