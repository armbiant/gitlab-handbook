---
title: IT Enterprise Applications Services
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## <i class="fas fa-users" id="biz-tech-icons"></i> About Us

Enterprise Application Services (EAS) provides  horizontal support, operations, administration, governance, risk assessment, and compliance of Enterprise Services. Leveraging the ITIL framework, EAS is responsible for the end-to-end service and application lifecycle management, and promotes operational excellence through IT Policies & Process Governance, including Change-, Release-, and Incident Management, while ensuring operating effectiveness through Compliance Reviews.

## <i class="fas fa-users" id="biz-tech-icons"></i> Mission

To provide frictionless, world class, Application Support Experience for the GitLab Team members by providing easy to use Service Management tools, serving as a single point of contact for all Enterprise Applications, mitigating risk through compliance, governance, change and release management. 

## <i class="fas fa-users" id="biz-tech-icons"></i> Roadmap

## <i class="fas fa-users" id="biz-tech-icons"></i> Our Team (Org Chart)

- **Carlo Curato - Director, Business Technology Operations** GitLab handle: [ccurato](https://gitlab.com/ccurato) Slack handle: @Carlo Curato
- **Derek Isla - Manager, IT Compliance** GitLab handle: [disla](https://gitlab.com/disla) Slack handle: @Derek Isla
- **Sarah Pang- Senior IT Compliance Engineer** GitLab handle: [sarahpang](https://gitlab.com/sarahpang) Slack handle: @Sarah Pang 
- **Kavya Cheppudira Nanjappa - Associate IT Compliance** Engineer GitLab handle: [KavyaNanjappa](https://gitlab.com/KavyaNanjappa) Slack handle: @knanjappa
- **David Kim - Business Technology Program Manager** GitLab handle: [dkim514](https://gitlab.com/dkim514) Slack handle: @David Kim
- **Bien Balaba - Business Technology Service Desk Analyst** GitLab handle: [BienRCB](https://gitlab.com/BienRCB) Slack handle: @Bien Balaba
- **Imran Khan- Senior IT Enterprise Applications Engineer** GitLab handle: [khan_imran](https://gitlab.com/khan_imran) Slack handle: @Imran Khan 
- **Marc DiSabatino - Business Systems Analyst** GitLab handle: [marc_disabatino](https://gitlab.com/marc_disabatino) Slack handle: @Marc Di Sabatino
- **Rey Manlangit- Associate IT Enterprise Applications Engineer** GitLab handle: [RManlangit](https://gitlab.com/RManlangit) Slack handle: @Rey Manlangit 
- **Xander Liwanag - Business Technology Service Desk Analyst** GitLab handle: [xliwanag](https://gitlab.com/xliwanag) Slack handle: @Xander Liwanag

## <i class="fas fa-users" id="biz-tech-icons"></i> Our Services

Service Operations

- Application LifeCycle Management
- Systems Administration
- Budget & Vendor Management
- Application Rationalization
- New Software Assessment and Approvals
- Change & Release Management

IT Compliance (SOX, GRC)

- ITGC
- External & Internal Audit Support
- User Access REview
- Separation of Duty

## <i class="fas fa-users" id="biz-tech-icons"></i> How We Operate

- [Agile Methodology as Operational Standard](/https://docs.google.com/presentation/d/1XAOalsNDOPvsunkMb2r5fwxdhRhhnNbzqdhwiXqxUaU/edit#slide=id.g12b319f6181_0_0).
- [Engagement Model](/https://docs.google.com/presentation/d/1slrn20jIenBnm-wbCs9Dv9gTDax11NnrXygCL7yMev4/edit#slide=id.g12b319f6181_0_0)
- [Major Incident Management](/https://docs.google.com/presentation/d/14F8-odTSEWQ0OS53oMJ-o6hDflplKkYsO4o9tww2uVw/edit#slide=id.p1) 
- Change Management (Coming Soon)
- Release Management (Coming Soon)
- New Application SOX Compliance Review (Coming Soon)
