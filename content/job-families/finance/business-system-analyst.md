---
title: "IT Business Systems"
---

The IT Business Systems job family at GitLab is in charge of designing, delivering, and maintaining high quality business  systems solutions by applying project management methodology.

## Levels

### Business Systems Specialist

#### Job Grade

The Business Systems Specialist is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Business Systems Specialist Responsibilities

- Is focused on the day-to-day activities and tasks that support the broader objectives of business systems analysis such as requirements elicitation, documentation, research, project & testing support.
- Plays a crucial role in assisting Senior Business Systems Analysts.
- Explains moderately complex issues and works to build alignment.
- Assists in collecting and documenting business requirements by conducting interviews and workshops with stakeholders. This involves actively listening to user needs and translating them into clear and concise requirements.
- Assists in creating and maintaining documentation, such as requirement specifications, process flows, data models, and user manuals.
- Conducts research on industry best practices, emerging technologies, and potential solutions to support senior analysts in making informed recommendations.
- Aids in the testing process by preparing test cases, participating in testing activities, and documenting test results. This may include functional, user acceptance, and regression testing.
- Learns and develops understanding of GitLab's way of working.
- Adapts to GitLab communication framework.
- Develops skills and behaviors aligned with GitLab Values.
- Has awareness of GitLab remote working best practices.

#### Business Systems Specialist Requirements

- Nice to have: Certification(s) related to business analysis or project management.
- Strong analytical thinking and problem-solving skills to identify and address business process issues and system inefficiencies.
- Effective communication skills, both written and verbal, to collaborate with various stakeholders, including business users, IT teams, and management.
- Familiarity with basic software development concepts and understanding of IT systems.

### Business Systems Analyst

#### Job Grade

The Business System Analyst is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Business Systems Analyst Responsibilities

- Collaborate with stakeholders to elicit, document, and analyze business requirements for new projects, system enhancements, or process improvements.
- Translate business requirements into detailed functional specifications for IT development teams.
- Create and maintain process flowcharts, data models, and other visual representations to illustrate current and proposed business processes.
- Assist in the design of technology solutions that align with business requirements.
- Work closely with engineers and architects to ensure that the proposed solutions meet business needs.
- Act as a liaison between business stakeholders and IT teams, ensuring clear communication and understanding of project objectives, status, and issues.
- Present findings and recommendations to GitLab management and executives in a clear and comprehensible manner.
- Track project progress, report on key milestones, and manage project-related issues and risks.
- Acts in alignment with GitLab communication framework.
- Grows skills aligned with GitLab Values.
- Understands the concepts behind GitLab remote work best practices.
- Regularly demonstrates, models and coaches other team members on GitLab's remote working competencies.

#### Business Systems Analyst Requirements

- 2-3 years of experience in Business Analysis or a related field. This experience should include hands-on experience with business analysis techniques and processes.
- Nice to have: Certification such as Certified Business Analysis Professional (CBAP), Certified ScrumMaster (CSM), or other relevant certifications.
- Strong analytical and critical-thinking skills to analyze complex business processes and systems, identify improvement opportunities, and develop effective solutions.
- Excellent written and verbal communication skills to interact with various stakeholders, including business users, IT teams, and senior management. This includes the ability to explain technical concepts to non-technical stakeholders.
- Proficiency in business analysis methodologies, tools, and techniques. This includes a solid understanding of software development concepts and IT systems.
- Familiarity with project management principles and practices to assist in project planning, monitoring, and coordination.
- Nice to have: Ability to write basic - medium complexity SQL queries.
- Nice to have: Familiarity with customer lifecycle tools and how they integrate: Marketo, Zuora, Salesforce, Zendesk, NetSuite, Coupa.
- Nice to have: Experience with SaaS products.
- Nice to have: Experience using GitLab/Git.

### Senior Business Systems Analyst

#### Job Grade

The Senior Business Systems Analyst is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Business Systems Analyst Responsibilities

- Participate in the development of the organization's strategic plans and objectives.
- Align technology solutions with long-term business goals and provide input on technology investments.
- Lead the gathering, documentation, and analysis of complex business requirements ensure they are clear, complete, and well-documented for development teams.
- Help stakeholders prioritize and manage changing requirements.
- Collaborate with technical teams to design high-level system architectures that meet business needs.
- Oversee the implementation of applications, coordinating with development and testing teams.
- Act as a trusted advisor to senior management and executives, providing insights and recommendations.
- Lead and oversee complex projects, including project planning, budgeting, and resource allocation.
- Mentor and guide junior analysts and project team members.
- Ensure that technology solutions and processes adhere to industry regulations and organizational governance standards.
- Models skills and behavior aligned with GitLab Values.
- Consistently demonstrates GitLab remote work best practices.
- Ability to align IT programs with the organization's strategic goals and vision, ensuring that they contribute to GitLab's overall success.

#### Senior Business Systems Analyst Requirements

Extends the Business Systems Analyst requirements plus:

- 3+ years of experience in Business Analysis or a related field. This experience should include hands-on experience with business analysis techniques and processes.

### Staff Business Systems Analyst

#### Job Grade

The Staff Business Systems Analyst is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Staff Business Systems Analyst Responsibilities

Extends the Senior Business Systems Analyst responsibilities plus:

- Ability to bridge the gap between business requirements and technical solutions while actively contributing to process improvement and organizational efficiency.
- Identify opportunities for process optimization and automation.
- Collaborate with third-party vendors and service providers to evaluate, select, and implement software solutions and services.
- Provide guidance and mentorship to junior analysts and team members.
- Collaborate with security and compliance teams to address vulnerabilities and mitigate risks.
- Coaches team members on skills and behaviors aligned with GitLab Values.
- Consistently demonstrates and educates other team members on GitLab remote work best practices.

#### Staff Business Systems Analyst Requirements

- 5+ years of experience in Business Analysis or a related field. This experience should include a proven track record of successfully leading and managing projects.
- Nice to have: Certification such as Certified Business Analysis Professional (CBAP), Certified ScrumMaster (CSM), or other relevant certifications.
- Strong analytical and critical-thinking skills are essential to understand complex business processes, identify improvement opportunities, and develop effective solutions.
- Excellent written and verbal communication skills are crucial for effectively interacting with business stakeholders, IT teams, and senior management. This includes the ability to convey technical information to non-technical audiences.
- Proficiency in business analysis methodologies, tools, and techniques.
- A solid understanding of software development concepts, and IT systems.
- An in-depth understanding of business processes, industry trends, and the ability to align technology solutions with long-term business goals.

### Principal Business Systems Analyst

#### Job Grade

The Principal Business Systems Analyst is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Principal Business Systems Analyst Responsibilities

Extends the Staff Business Systems Analyst responsibilities plus:

- Participate in the development of the organization's technology and business strategies, aligning technology solutions with long-term goals.
- Provide strategic input on technology investments and initiatives.

#### Principal Business Systems Analyst Requirements

Extends the Staff, Business Systems Analyst requirements plus:

- 10+ years of experience in Business Analysis or a related field. This experience should include a proven track record of successfully leading and managing projects.
- Strong leadership abilities, as Principal Business Systems Analysts often lead and mentor teams, oversee projects, and drive organizational change.
- The ability to provide guidance, mentorship, and coaching to junior analysts and team members.

### Distinguished Business Systems

#### Job Grade

The Distinguished Business Systems is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Distinguished Business Systems Responsibilities

Extends the Principal Business Systems Analyst responsibilities plus:

- Act as a key advisor to the IT executive team and board of directors on technology-related matters.
- Establish and maintain effective communication channels with C-suite executives and other senior leaders, conveying complex technical concepts in a clear and understandable manner.
- Develop and manage technology roadmaps, ensuring that technology investments align with the organization's strategic objectives.

#### Distinguished Business Systems Architect

Extends the Principal, Business Systems Analyst requirements plus:

- 12-15 years of experience in Business Analysis or a related field. This experience should include a proven track record of successfully leading and managing projects.
- Proven leadership capabilities, including the ability to provide strategic vision and guidance to the organization.
- A reputation as a thought leader in the field of business analysis, evidenced by contributions to industry conferences, publications, and the advancement of best practices.

## Performance Indicators (PI)

- [Evaluating System or Process Efficiency](https://internal.gitlab.com/handbook/it/it-performance-indicators/#evaluating-system-or-process-efficiency)
- [Average Issues](https://internal.gitlab.com/handbook/it/it-performance-indicators/#average-issues)
- [Average Merge Requests](https://internal.gitlab.com/handbook/it/it-performance-indicators/#average-merge-request)

## Career Ladder

Any IT team member can explore lateral career moves within the broader IT job family, provided they fulfill the responsibilities and requirements associated with the desired role.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/handbook/company/team/).

- Next, candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
- Next, candidates will be invited to schedule a first interview with our Business Systems Analysts in a panel interview
- Next, candidates will be invited to schedule a second interview with our IT Operations Team (may be a panel)
- Candidates will then be invited to schedule a third interview with a Team Member (may be member of the SalesOps, MarketingOps, or Data Ops team)
- Then the candidate will be invited to interview with the Director of Enterprise Applications
- Finally, candidates may be asked to interview with our CFO or CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring/)
